#!/bin/bash

cp ../punsarn-setup.sh bin/

docker run -it \
    -v ./bin:/tmp/bin \
    -v ./site:/tmp/site \
    --rm debian:11 /bin/bash
