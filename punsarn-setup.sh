#!/bin/bash

#### PUNSARN: SCRIPT TO SETUP SERVER ####

NAME="punsarn-setup"
VERSION="1.0"

DEFAULT_PACKAGES="unattended-upgrades sudo screen htop nmon iotop vim locales tzdata acl procps"

WORKING_DIR=/srv/punsarn

LOG=/var/log/$NAME.log

if [ "$EUID" -ne 0 ]; then
    echo "Please run as root"
    exit
fi

echo "####### PUNSARN: SCRIPT TO SETUP SERVER ########" >$LOG

if [ -f /var/log/$NAME.init ]; then
    echo "Update Date: $(date)" >>$LOG
    echo "Version: $VERSION" >>$LOG
else
    echo "Last update: $(date)" >/var/log/$NAME.init
    echo "Version: $VERSION" >>/var/log/$NAME.init
fi
echo "-------------------------------------------------" >>$LOG

install_default_packages() {
    apt update
    apt install -y $DEFAULT_PACKAGES
}

list_upgradable_packages() {
    if [[ $(apt list --upgradable 2>/dev/null | sed '$ d' | wc -l) -gt 0 ]]; then
        echo "Upgradable packages" >>$LOG
        apt list --upgradable 2>/dev/null | sed '$ d' >>$LOG
    fi
    echo "-------------------------------------------------" >>$LOG
}

set_timezone_and_locales() {
    ln -sf /usr/share/zoneinfo/Asia/Bangkok /etc/localtime
    dpkg-reconfigure -f noninteractive tzdata

    sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
    sed -i -e 's/# th_TH.UTF-8 UTF-8/th_TH.UTF-8 UTF-8/' /etc/locale.gen
    dpkg-reconfigure --frontend=noninteractive locales
}

initial_working_dir() {
    if [ -d $WORKING_DIR ]; then
        echo "Working dir exists"
    else
        mkdir --mode=u+rwx,g+wrs,o-w $WORKING_DIR
    fi

    if [ $(getent group punsarn) ]; then
        echo "group punsarn exists."
    else
        addgroup punsarn
    fi

    # Set permission and ACL
    setfacl -d -m g:punsarn:wr $WORKING_DIR

    chgrp punsarn /srv/punsarn $WORKING_DIR
}

validate_working_dir() {
    SITE=$WORKING_DIR/site

    if ! [ -d $SITE ]; then
        echo "Site not found. $SITE"
        return 0
    fi

    # update permission and group
    chgrp -R punsarn $SITE
    find $SITE -type d -exec chmod g+wrs {} \;

    # update important file that need specific permission and owner

    # config-*
    if [ -d $SITE/config-cron ]; then
        chown root:root $SITE/config-cron/*
        chmod 644 $SITE/config-cron/*
    fi

    if [ -d $SITE/config-logrotate ]; then
        chown root:root $SITE/config-logrotate/*
        chmod 644 $SITE/config-logrotate/*
    fi

    if [ -d $SITE/config-rsyslog ]; then
        chown root:root $SITE/config-rsyslog/*
    fi
}

validate_backup() {
    validate_backup_koha
    validate_backup_dspace
}

validate_backup_koha() {
    if [ -d /usr/share/koha ]; then
        BACKUP_DIR=/var/spool/koha
        for INSTANCE in $(/usr/sbin/koha-list); do
            BACKUP_LAST=$(ls -Art $BACKUP_DIR/$INSTANCE/*.sql.* | tail -n 1)
            echo "Last backup DB: $BACKUP_LAST" >>$LOG
        done
        echo "-------------------------------------------------" >>$LOG
    fi
}

validate_backup_dspace() {
    if [ -d /srv/punsarn/dspace ]; then
        BACKUP_DIR=/var/spool/dspace
        BACKUP_LAST=$(ls -Art $BACKUP_DIR/*.sql.* | tail -n 1)
        echo "Last backup DB: $BACKUP_LAST" >>$LOG
        echo "-------------------------------------------------" >>$LOG
    else
        echo ""
    fi
}

status_info() {
    # uptime disk space
    echo "Uptime: $(uptime)" >>$LOG
    echo "Disk space:" >>$LOG
    df -h | grep -v "^tmpfs" >>$LOG
    echo "-------------------------------------------------" >>$LOG
}

validate_logging() {
    LARGE_LOG_FILES=$(find /var/log -type f -size +500M -exec ls -lah {} \;)
    LARGE_LOG_COUNT=$(find /var/log -type f -size +500M | wc -l)

    if [ $LARGE_LOG_COUNT -gt 0 ]; then
        echo "[WARNING] found large log files" >>$LOG
        echo $LARGE_LOG_FILES >>$LOG
    fi
    echo "-------------------------------------------------" >>$LOG
}

install_default_packages
set_timezone_and_locales
initial_working_dir
list_upgradable_packages
validate_working_dir
validate_backup
validate_logging
status_info

echo "################ + OUTPUT + #####################"
cat $LOG
