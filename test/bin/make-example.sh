#!/bin/bash

cp -r /tmp/site /srv/punsarn/ 

mkdir -p /srv/punsarn/dspace
mkdir -p /var/spool/dspace

touch /var/spool/dspace/mu-2023-05-11.sql.gz

mkdir -p /usr/share/koha
mkdir -p /var/spool/koha/pim
touch /var/spool/koha/pim/pim-2023-05-11.sql.gz

echo "echo \"pim\"" > /usr/sbin/koha-list
chmod +x /usr/sbin/koha-list

dd if=/dev/zero of=/var/log/test.log bs=1024 count=0 seek=$[1024*600]
